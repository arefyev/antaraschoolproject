import animals.*;
import food.*;

import java.util.ArrayList;
import java.util.Arrays;

public class Zoo {

    public static void main(String[] args) {
        Worker worker = new Worker();
        Duck duck = new Duck(10, "Ферма", 10, "Кря кря кря", 5, true, "Дональд");
        Fish fish  = new Fish(5, "Озеро", 25, 1, false, "Дори");
        Deer deer = new Deer(3, "Лес", 80, "Стон", 300, false, "Рудольф");
        Herbivore[] prey = {duck, fish, deer};
        ArrayList<Herbivore> animalList = new ArrayList<>(Arrays.asList(prey));
        Bear bear = new Bear(1, "Лес", 40, "Рык", 350, animalList, "Винни-пух");
        Lion lion  = new Lion(2, "Саванна", 50, "Рёв", 250, animalList, "Симба");
        Wolf wolf = new Wolf(4, "Лес", 50, "Вой", 35, animalList, "Серый");
        Swim[] pond = {duck, fish, deer, bear, lion, wolf};
        Food beef = new Beef(100);
        Food chicken = new Chicken( 100);
        Food pork = new Pork( 100);
        Food burdock = new Burdock( 100);
        Food clover = new Clover( 100);

        worker.feed(bear, burdock);
        worker.feed(lion, beef);
        worker.feed(duck, pork);
        worker.feed(fish, clover);
        lion.hunting();
        fish.pluckGrass();

        System.out.println("Сытость льва " + lion.getSatiety());
        System.out.println("Общее Количество калорий курицы " + chicken.countCalories());
        System.out.println(worker.getVoice(duck));
        System.out.println(worker.getVoice(deer));
        System.out.println(worker.getVoice(bear));
        System.out.println(worker.getVoice(lion));
        System.out.println(worker.getVoice(wolf));

        for (Swim p : pond) {
            p.swim();
        }

        try {
            System.out.println(worker.getVoice((Voice) fish));
        } catch (ClassCastException ex) {
            System.out.println("Животное не умеет разговаривать");
        }
    }
}
