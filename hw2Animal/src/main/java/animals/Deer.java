package animals;

public class Deer extends Herbivore implements  Swim, Voice, Run{
    private String voice;

    public Deer() {
    }

    public Deer(int hunger, String location, int speed, String voice, int weight, boolean pet, String name) {
        this.setSatiety(hunger);
        this.setLocation(location);
        this.setSpeed(speed);
        this.voice = voice;
        this.setWeight(weight);
        this.setPet(pet);
        this.setName(name);
    }

    @Override
    public void run() {
        System.out.println("Олень очень быстро бегает");
    }

    @Override
    public void swim() {
        System.out.println("Олень куда-то поплыл");
    }

    @Override
    public String voice() {
        return voice;
    }

    public void setVoice(String voice) {
        this.voice = voice;
    }
}
