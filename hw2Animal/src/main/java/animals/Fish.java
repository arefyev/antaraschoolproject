package animals;

public class Fish extends Herbivore implements Swim{

    public Fish() {
    }

    public Fish(int hunger, String location, int speed, int weight, boolean pet, String name) {
        this.setSatiety(hunger);
        this.setLocation(location);
        this.setSpeed(speed);
        this.setWeight(weight);
        this.setPet(pet);
        this.setName(name);
    }

    @Override
    public void pluckGrass() {
        if (this.getPet())
            System.out.println(this.getName() + " поела водные растения в аквариуме");
        else
            System.out.println(this.getName() + " поела водоросли в пруду");
    }

    @Override
    public void swim() {
        System.out.println("Эта рыба плавает лучше остальных животных");
    }

    @Override
    public void roam() {
        System.out.println("На далёком севере бродит рыба кит!");
    }
}
