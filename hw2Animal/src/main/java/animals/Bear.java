package animals;

import java.util.ArrayList;

public class Bear extends Carnivorous implements Run, Swim, Voice{
    private String voice;

    public Bear() {
    }

    public Bear(int hunger, String location, int speed, String voice, int weight, ArrayList<Herbivore> animalList, String name) {
        this.setSatiety(hunger);
        this.setLocation(location);
        this.setSpeed(speed);
        this.voice = voice;
        this.setWeight(weight);
        this.setPreyList(animalList);
        this.setName(name);
    }

    @Override
    public void run() {
        if(this.getSatiety() > 10)
            System.out.println("Медведь бегает по полю");
        else
            System.out.println("Мишка слишком голоден, чтобы бегать");
    }

    @Override
    public void swim() {
        System.out.println("Медведь поплыл за лососем");
    }

    @Override
    public String voice() {
        return voice;
    }

    public void setVoice(String voice) {
        this.voice = voice;
    }
}
