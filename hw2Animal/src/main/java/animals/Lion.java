package animals;

import java.util.ArrayList;

public class Lion extends Carnivorous implements Run, Swim, Voice{
    private String voice;

    public Lion() {
    }

    public Lion(int hunger, String location, int speed, String voice, int weight, ArrayList<Herbivore> animalList, String name) {
        this.setSatiety(hunger);
        this.setLocation(location);
        this.setSpeed(speed);
        this.voice = voice;
        this.setWeight(weight);
        this.setPreyList(animalList);
        this.setName(name);
    }

    @Override
    public void run() {
        if(this.getSatiety() < 10)
            System.out.println("Лев бегает за добычей");
        else
            System.out.println("Лев сыт и не хочет куда-то бежать");
    }

    @Override
    public void swim() {
        System.out.println("Лев поплыл на другую сторону озера");
    }

    @Override
    public String voice() {
        return voice;
    }

    public void setVoice(String voice) {
        this.voice = voice;
    }
}
