package animals;

import food.Food;

public abstract class Animal {
    private int satiety = 0;
    private String location;
    private int speed;
    private int weight;

    private String name;

    abstract public void eat(Food food);

    public void roam() {
        if(weight < 500) {
            System.out.println("Животное долго бродит по " + location);
        } else {
            System.out.println("Животное побродило и быстро устало");
        }
    }

    public void sleep() {
        System.out.println("Животное выспалось");
    }

    public int getSatiety() {
        return satiety;
    }

    public void setSatiety(int satiety) {
        this.satiety += satiety;
    }
    public void setHunger() {
        this.satiety = 0;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
