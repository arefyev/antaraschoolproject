package animals;

import food.Food;
import food.Grass;

public abstract class Herbivore extends Animal {
    private boolean pet;

    public void pluckGrass() {
        if (pet)
            System.out.println("Животное пощипало капусту");
        else
            System.out.println("Животное пощипало травку");
    }

    @Override
    public void eat(Food food) {
        if (food instanceof Grass) {
            this.setSatiety(food.countCalories() / 10);
            System.out.println("Животное съело траву");
        }
        else
            System.out.println("Я такое не ем, можно мне травы?!!");
    }

    public boolean getPet() {
        return this.pet;
    }

    public void setPet(boolean pet) {
        this.pet = pet;
    }
}
