package animals;

import food.Food;
import food.Meat;

import java.util.ArrayList;

public abstract class Carnivorous extends Animal{
    private ArrayList<Herbivore> preyList = new ArrayList<>();

    public void hunting() {
        int animal = (int)(Math.random() * preyList.size());

        if ((this.getSpeed() + this.getSatiety()) > (preyList.get(animal).getSpeed() + preyList.get(animal).getSatiety()))
            System.out.println("Охота завершилась удачно, удалось поймать " + preyList.get(animal).getName());
        else
            System.out.println("Не хватило скорости, чтобы поймать " + preyList.get(animal).getName());
    }

    @Override
    public void eat(Food food) {
        if (food instanceof Meat) {
            this.setSatiety(food.countCalories()/10);
            System.out.println("Животное съело мясо");
        }
        else
            System.out.println("Я ХИЩНИК и траву не ем!!!");
    }

    public ArrayList<Herbivore> getPreyList() {
        return preyList;
    }

    public void setPreyList(ArrayList<Herbivore> preyList) {
        this.preyList = preyList;
    }
}
