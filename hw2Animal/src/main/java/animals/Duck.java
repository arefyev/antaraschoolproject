package animals;

public class Duck extends Herbivore implements  Fly, Swim, Voice, Run{
    private String voice;

    public Duck() {
    }

    public Duck(int hunger, String location, int speed, String voice, int weight, boolean pet, String name) {
        this.setSatiety(hunger);
        this.setLocation(location);
        this.setSpeed(speed);
        this.voice = voice;
        this.setWeight(weight);
        this.setPet(pet);
        this.setName(name);
    }

    @Override
    public void fly() {
        System.out.println("Утка улетела");
    }

    @Override
    public void run() {
        System.out.println("Утка бегает очень медленно");
    }

    @Override
    public void swim() {
        System.out.println("Утка поплыла за рыбой");
    }

    @Override
    public String voice() {
        return voice;
    }

    public void setVoice(String voice) {
        this.voice = voice;
    }
}
