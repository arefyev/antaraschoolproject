package animals;

import java.util.ArrayList;

public class Wolf extends Carnivorous implements Run, Swim, Voice{
    private String voice;

    public Wolf() {
    }

    public Wolf(int hunger, String location, int speed, String voice, int weight, ArrayList<Herbivore> animalList, String name) {
        this.setSatiety(hunger);
        this.setLocation(location);
        this.setSpeed(speed);
        this.voice = voice;
        this.setWeight(weight);
        this.setPreyList(animalList);
        this.setName(name);
    }

    @Override
    public void run() {
        if(this.getSpeed() > 50)
            System.out.println("Волк пробежал больше 50 км за час");
        else
            System.out.println("Волк пробежал меньше 50 км за час");
    }

    @Override
    public void swim() {
        System.out.println("Волк проплыл 13 км!");
    }

    @Override
    public String voice() {
        return voice;
    }

    public void setVoice(String voice) {
        this.voice = voice;
    }
}
