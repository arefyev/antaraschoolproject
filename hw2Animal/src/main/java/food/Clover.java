package food;

public class Clover extends Grass {
    private final static float ONE_CALORIES_FROM_GRAMM = 0.03f;

    public Clover() {
    }

    public Clover(int amountGrass) {
        this.setAmountGrass(amountGrass);
    }

    @Override
    public int countCalories() {
        return (int)(this.getAmountGrass()*ONE_CALORIES_FROM_GRAMM);
    }
}
