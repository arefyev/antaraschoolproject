package food;

public class Beef extends Meat{
    private final static float ONE_CALORIES_FROM_GRAMM = 1.87f;

    public Beef() {
    }

    public Beef(int amountMeat) {
        this.setAmountMeat(amountMeat);
    }

    @Override
    public int countCalories() {
        return (int)(this.getAmountMeat()*ONE_CALORIES_FROM_GRAMM);
    }
}
