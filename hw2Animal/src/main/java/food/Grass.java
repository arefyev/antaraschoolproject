package food;

public abstract class Grass extends Food{
    private int amountGrass;

    public boolean checkStocks() {
        if(this.amountGrass < 100) {
            System.out.println("Нужно собрать траву");
            return false;
        }
        System.out.println("Травы достаточно");
        return true;
    }

    public void stockFood(){
        this.amountGrass = this.amountGrass + 100;
    }

    public void stockFood(int amountGrass){
        this.amountGrass = this.amountGrass + amountGrass;
    }

    public int getAmountGrass() {
        return amountGrass;
    }

    public void setAmountGrass(int amountGrass) {
        this.amountGrass = amountGrass;
    }
}
