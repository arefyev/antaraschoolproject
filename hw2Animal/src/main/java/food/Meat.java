package food;

public abstract class Meat extends Food{
    private int amountMeat;

    public boolean checkStocks() {
        if(this.amountMeat < 100) {
            System.out.println("Нужно купить мясо");
            return false;
        }
        System.out.println("Мяса достаточно");
        return true;
    }

    @Override
    public void stockFood() {
        this.amountMeat = this.amountMeat + 100;
    }

    public void stockFood(int amountMeat){
        this.amountMeat = this.amountMeat + amountMeat;
    }

    public int getAmountMeat() {
        return this.amountMeat;
    }

    public void setAmountMeat(int amountMeat) {
        this.amountMeat = amountMeat;
    }
}
