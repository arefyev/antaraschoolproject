package food;

public abstract class Food{

    public abstract int countCalories();

    public abstract boolean checkStocks();

    public abstract void stockFood();
}
