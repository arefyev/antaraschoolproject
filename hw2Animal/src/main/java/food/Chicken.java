package food;

public class Chicken extends Meat{
    private final static float ONE_CALORIES_FROM_GRAMM = 1.9f;

    public Chicken() {
    }

    public Chicken(int amountMeat) {
        this.setAmountMeat(amountMeat);
    }

    @Override
    public int countCalories() {
        return (int)(this.getAmountMeat()*ONE_CALORIES_FROM_GRAMM);
    }
}
