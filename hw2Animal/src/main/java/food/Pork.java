package food;

public class Pork extends Meat{
    private final static float ONE_CALORIES_FROM_GRAMM = 2.59f;

    public Pork() {
    }

    public Pork(int amountMeat) {
        this.setAmountMeat(amountMeat);
    }

    @Override
    public int countCalories() {
        return (int)(this.getAmountMeat()*ONE_CALORIES_FROM_GRAMM);
    }
}
