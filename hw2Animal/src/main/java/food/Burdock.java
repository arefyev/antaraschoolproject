package food;

public class Burdock extends Grass{
    private final static float ONE_CALORIES_FROM_GRAMM = 0.25f;

    public Burdock() {
    }

    public Burdock( int amountGrass) {
        this.setAmountGrass(amountGrass);
    }

    @Override
    public int countCalories() {
        return (int)(this.getAmountGrass()*ONE_CALORIES_FROM_GRAMM);
    }
}
