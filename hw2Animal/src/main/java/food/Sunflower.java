package food;

public class Sunflower extends Grass {
    private final static float ONE_CALORIES_FROM_GRAMM = 5.78f;

    public Sunflower() {
    }

    public Sunflower(int amountGrass) {
        this.setAmountGrass(amountGrass);
    }

    @Override
    public int countCalories() {
        return (int)(this.getAmountGrass()*ONE_CALORIES_FROM_GRAMM);
    }
}
