--1. Вывести список самолетов с кодами 320, 321, 733;
SELECT *
FROM lanit.AIRCRAFTS_DATA ad 
WHERE ad.AIRCRAFT_CODE  IN ('320', '321', '733');

--2. Вывести список самолетов с кодом не на 3;
SELECT * 
FROM lanit.AIRCRAFTS_DATA ad 
WHERE ad.AIRCRAFT_CODE NOT LIKE '3%';

--3. Найти билеты оформленные на имя «OLGA», с емайлом «OLGA» или без емайла;
SELECT *
FROM lanit.TICKETS t 
WHERE t.PASSENGER_NAME LIKE '%OLGA%'
AND (UPPER(t.EMAIL) LIKE '%OLGA%' OR t.EMAIL IS NULL);

--4. Найти самолеты с дальностью полета 5600, 5700. Отсортировать список по убыванию дальности полета;
SELECT * 
FROM lanit.AIRCRAFTS_DATA ad 
WHERE ad.RANGE IN(5600, 5700)
ORDER BY ad.RANGE DESC;

--5. Найти аэропорты в Moscow. Вывести название аэропорта вместе с городом. Отсортировать по полученному названию;
SELECT ad.AIRPORT_NAME, ad.CITY 
FROM lanit.AIRPORTS_DATA ad
WHERE ad.CITY = 'Moscow'
ORDER BY ad.AIRPORT_NAME ;

--6. Вывести список всех городов без повторов в зоне «Europe»;
SELECT ad.CITY
FROM lanit.AIRPORTS_DATA ad
WHERE ad.TIMEZONE LIKE '%Europe%'
GROUP BY ad.CITY;

--7. Найти бронирование с кодом на «3A4» и вывести сумму брони со скидкой 10%
SELECT TOTAL_AMOUNT * 0.9 AS TOTAL_AMOUNT_SALE_10 
FROM lanit.BOOKINGS b 
WHERE b.BOOK_REF LIKE '3A4%';

--8. Вывести все данные по местам в самолете с кодом 320 и классом «Business» строками вида «Данные по месту: номер места 1», «Данные по месту: номер места 2» … и тд;
SELECT 'Location data: location number ' || s.SEAT_NO ||' '|| s.FARE_CONDITIONS ||' '|| s.AIRCRAFT_CODE AS LOCATION_DATA 
FROM lanit.SEATS s 
WHERE s.AIRCRAFT_CODE = '320'
AND s.FARE_CONDITIONS = 'Business';

--9. Найти максимальную и минимальную сумму бронирования в 2017 году;
SELECT MAX(b.TOTAL_AMOUNT) AS MAX_TOTAL_AMOUNT, MIN(b.TOTAL_AMOUNT) AS MIN_TOTAL_AMOUNT
FROM lanit.BOOKINGS b
WHERE trunc(b.BOOK_DATE)
BETWEEN TO_DATE('2017-01', 'yyyy-mm')
AND TO_DATE('2017-12', 'yyyy-mm');

--10. Найти количество мест во всех самолетах, вывести в разрезе самолетов;
SELECT s.AIRCRAFT_CODE, COUNT(s.AIRCRAFT_CODE) AS TOTAL_NUMBER_SEATS
FROM lanit.SEATS s
GROUP BY s.AIRCRAFT_CODE;

--11. Найти количество мест во всех самолетах с учетом типа места, вывести в разрезе самолетов и типа мест;
SELECT s.AIRCRAFT_CODE, s.FARE_CONDITIONS, COUNT(s.FARE_CONDITIONS) AS TOTAL_TYPE
FROM lanit.SEATS s
GROUP BY s.FARE_CONDITIONS, s.AIRCRAFT_CODE
ORDER BY s.AIRCRAFT_CODE DESC, s.AIRCRAFT_CODE DESC;

--12. Найти количество билетов пассажира ALEKSANDR STEPANOV, телефон которого заканчивается на 11;
SELECT COUNT(t.PASSENGER_NAME) AS TOTAL_TICKETS_ALEKSANDR_STEPANOV
FROM lanit.TICKETS t
WHERE t.PASSENGER_NAME = 'ALEKSANDR STEPANOV'
AND t.PHONE LIKE '%11'
GROUP BY t.PASSENGER_NAME;

--13. Вывести всех пассажиров с именем ALEKSANDR, у которых количество билетов больше 2000. Отсортировать по убыванию количества билетов;
SELECT t.PASSENGER_NAME
FROM lanit.TICKETS t
WHERE t.PASSENGER_NAME LIKE 'ALEKSANDR%'
GROUP BY t.PASSENGER_NAME
HAVING COUNT(t.TICKET_NO) > 2000
ORDER BY COUNT(t.TICKET_NO) DESC;

--14. Вывести дни в сентябре 2017 с количеством рейсов больше 500.
SELECT EXTRACT(DAY FROM f.SCHEDULED_DEPARTURE) AS DAY_OF_SEPTEMBER
FROM lanit.FLIGHTS f 
WHERE trunc(f.SCHEDULED_DEPARTURE) 
BETWEEN TO_DATE('2017-09-01', 'yyyy-mm-dd') 
AND TO_DATE('2017-09-30', 'yyyy-mm-dd') 
GROUP BY EXTRACT(DAY FROM f.SCHEDULED_DEPARTURE)
HAVING COUNT(EXTRACT(DAY FROM f.SCHEDULED_DEPARTURE)) > 500
ORDER BY EXTRACT(DAY FROM f.SCHEDULED_DEPARTURE);

--15. Вывести список городов, в которых несколько аэропортов;
SELECT ad.CITY
FROM LANIT.AIRPORTS_DATA ad 
GROUP BY ad.CITY
HAVING COUNT(ad.AIRPORT_CODE) > 1;

--16. Вывести модель самолета и список мест в нем, т.е. на самолет одна строка результата;
SELECT s.AIRCRAFT_CODE, COUNT(s.SEAT_NO) AS SEATS
FROM LANIT.SEATS s 
GROUP BY s.AIRCRAFT_CODE;

--17. Вывести информацию по всем рейсам из аэропортов в г.Москва за сентябрь 2017;
SELECT * 
FROM lanit.AIRPORTS_DATA ad
JOIN lanit.FLIGHTS f 
ON ad.AIRPORT_CODE = f.DEPARTURE_AIRPORT 
WHERE ad.CITY = 'Moscow' 
AND trunc(f.SCHEDULED_DEPARTURE) 
BETWEEN TO_DATE('2017-09-01', 'yyyy-mm-dd') 
AND TO_DATE('2017-09-30', 'yyyy-mm-dd');

--18. Вывести кол-во рейсов по каждому аэропорту в г.Москва за 2017;
SELECT ad.AIRPORT_CODE, COUNT(ad.AIRPORT_CODE) AS NUMBER_OF_FLIGHTS 
FROM lanit.AIRPORTS_DATA ad
JOIN lanit.FLIGHTS f 
ON ad.AIRPORT_CODE = f.DEPARTURE_AIRPORT 
WHERE ad.CITY = 'Moscow' 
AND EXTRACT(YEAR FROM f.SCHEDULED_DEPARTURE) = 2017
GROUP BY ad.AIRPORT_CODE;

--19. Вывести кол-во рейсов по каждому аэропорту, месяцу в г.Москва за 2017
SELECT  ad.AIRPORT_CODE, EXTRACT(MONTH FROM f.SCHEDULED_DEPARTURE) AS MONTH_2017, COUNT(ad.AIRPORT_CODE) AS NUMBER_OF_FLIGHTS 
FROM lanit.AIRPORTS_DATA ad
JOIN lanit.FLIGHTS f 
ON ad.AIRPORT_CODE = f.DEPARTURE_AIRPORT 
WHERE ad.CITY = 'Moscow' 
AND trunc(f.SCHEDULED_DEPARTURE) 
BETWEEN TO_DATE('2017-01', 'yyyy-mm')
AND TO_DATE('2017-12', 'yyyy-mm')
GROUP BY ad.AIRPORT_CODE, EXTRACT(MONTH FROM f.SCHEDULED_DEPARTURE)
ORDER BY ad.AIRPORT_CODE, EXTRACT(MONTH FROM f.SCHEDULED_DEPARTURE);

--20. Найти все билеты по бронированию на «3A4B»;
SELECT  *
FROM lanit.TICKETS t 
WHERE t.BOOK_REF LIKE '3A4B%';

--21. Найти все перелеты по бронированию на «3A4B»;
SELECT  *
FROM lanit.FLIGHTS f
JOIN lanit.TICKET_FLIGHTS tf 
ON tf.FLIGHT_ID = f.FLIGHT_ID
JOIN lanit.TICKETS t
ON t.TICKET_NO = tf.TICKET_NO
WHERE t.BOOK_REF LIKE '3A4B%';