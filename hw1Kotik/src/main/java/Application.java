import model.Kotik;

public class Application {
    public static void main(String[] args) {
        Kotik kotikVasya = new Kotik(8, "Vasya", 5, "Mяяяу, мяу!");
        Kotik kotikPushok = new Kotik();
        kotikPushok.setKotik(10, "Pushok", 7, "Mя, мя, мя, мяу!");
        kotikVasya.liveAnotherDay();
        System.out.println(kotikVasya.getName());
        System.out.println(kotikVasya.getWeight());
        System.out.println(kotikVasya.getMeow().equals(kotikPushok.getMeow()));
        System.out.println(Kotik.getKotikCount());
    }
}
