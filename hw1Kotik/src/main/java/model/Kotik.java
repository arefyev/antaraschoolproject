package model;

public class Kotik {
    private int satiety = (int) (Math.random() * 30);
    private int prettiness;
    private String name;
    private int weight;
    private String meow;
    private static int kotikCount = 0;
    private int countAction = 0;
    private int countEat = 0;

    public Kotik() {
        kotikCount++;
    }

    public Kotik(int prettiness, String name, int weight, String meow) {
        this();
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
    }

    public boolean play() {
        if(satiety-4 >= 0) {
            satiety -= 4;
            countAction++;
            System.out.println("Котик выполнил задание №" + countAction + ": Котик поиграл");
            return true;
        }
        return false;
    }

    public boolean sleep() {
        if(satiety-3 >= 0) {
            satiety -= 3;
            countAction++;
            System.out.println("Котик выполнил задание №" + countAction + ": Котик поспал");
            return true;
        }
        return false;
    }

    public boolean chaseMouse(){
        if(satiety-5 >= 0) {
            satiety -= 5;
            countAction++;
            System.out.println("Котик выполнил задание №" + countAction + ": Котик погонялся за мышью");
            return true;
        }
        return false;
    }

    public boolean washUp() {
        if(satiety-2 >= 0) {
            satiety -= 2;
            countAction++;
            System.out.println("Котик выполнил задание №" + countAction + ": Котик умылся");
            return true;
        }
        return false;
    }

    public boolean purr() {
        if(satiety-1 >= 0) {
            satiety -= 1;
            countAction++;
            System.out.println("Котик выполнил задание №" + countAction + ": Котик помурлыкал");
            return true;
        }
        return false;
    }

    public void liveAnotherDay() {
        for (int i = 0; i < 24; i++) {
            int methodNumber = (int) (Math.random() * 5 + 1);
            boolean currentSatiety = false;
            switch (methodNumber) {
                case 1:
                    currentSatiety = play();
                    break;
                case 2:
                    currentSatiety = sleep();
                    break;
                case 3:
                    currentSatiety = chaseMouse();
                    break;
                case 4:
                    currentSatiety = washUp();
                    break;
                case 5:
                    currentSatiety = purr();
                    break;
            }
            if (!currentSatiety) {
                countEat++;
                eat();
            }
        }
    }

    public void eat(int feed) {
        satiety += feed;
    }

    public void eat(int feed, String title) {
        satiety += feed;
        System.out.println("Количество перекусов котика " + countEat + ": Кормим котика " + title);
    }

    public void eat() {
        eat(35, "kitekat");
    }

    public int getSatiety() {
        return satiety;
    }

    public int getPrettiness() {
        return prettiness;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public String getMeow() {
        return meow;
    }

    public static int getKotikCount() {
        return kotikCount;
    }

    public void setKotik(int prettiness, String name, int weight, String meow) {
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
    }

    public int getCountAction() {
        return countAction;
    }

    public int getCountEat() {
        return countEat;
    }

    public void setSatiety(int satiety) {
        this.satiety = satiety;
    }

    public void setPrettiness(int prettiness) {
        this.prettiness = prettiness;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setMeow(String meow) {
        this.meow = meow;
    }

    public void setCountAction(int countAction) {
        this.countAction = countAction;
    }

    public void setCountEat(int countEat) {
        this.countEat = countEat;
    }
}
